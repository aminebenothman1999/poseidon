import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './login/main/main.component';
import { RegisterComponent } from './register/register.component';
import { MainRegisterComponent } from './register/main-register/main-register.component';
import { Footer1Component } from './footer1/footer1.component';
import { LeftNavigationComponent } from './left-navigation/left-navigation.component';
import { HeaderComponent } from './header/header.component';

const appRoutes: Routes=[
  {path:'',component:LoginComponent},
    //{path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    RegisterComponent,
    MainRegisterComponent,
    Footer1Component,
    LeftNavigationComponent,
    HeaderComponent,
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
